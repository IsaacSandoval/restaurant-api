import { SearchService } from './search.service';
export declare class SearchController {
    private searchService;
    constructor(searchService: SearchService);
    getResults(termino: string): Promise<any[]>;
}
