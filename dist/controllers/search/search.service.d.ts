import { Model } from 'mongoose';
export declare class SearchService {
    private readonly foodModel;
    constructor(foodModel: Model<any>);
    getResults(termino: string): Promise<any[]>;
}
