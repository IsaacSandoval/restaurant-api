import { Model } from 'mongoose';
import { FoodDTO } from './DTO/foodDTO';
export declare class FoodService {
    private readonly foodModel;
    constructor(foodModel: Model<any>);
    getFood(): Promise<any[]>;
    getFoodById(idFood: string): Promise<any>;
    createFood(food: FoodDTO): Promise<any>;
    deleteFood(idFood: string): Promise<any>;
    updateFood(idFood: string, foodUpdated: FoodDTO): Promise<any>;
}
