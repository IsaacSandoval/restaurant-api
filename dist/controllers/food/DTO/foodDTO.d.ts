export declare class FoodDTO {
    readonly name: string;
    readonly price: number;
    readonly description: string;
    readonly categorie: number;
    readonly img: string;
}
