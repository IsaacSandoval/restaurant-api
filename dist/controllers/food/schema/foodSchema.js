"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.FoodSchema = new mongoose.Schema({
    name: String,
    price: Number,
    description: String,
    categorie: Number,
    img: String,
});
//# sourceMappingURL=foodSchema.js.map