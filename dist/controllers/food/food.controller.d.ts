import { FoodService } from './food.service';
import { FoodDTO } from './DTO/foodDTO';
export declare class FoodController {
    private foodService;
    constructor(foodService: FoodService);
    getFood(): Promise<any[]>;
    getFoodByID(idFood: string): Promise<any>;
    createFood(food: FoodDTO): Promise<any>;
    deleteFood(id: string): Promise<any>;
    updateFood(idFood: string, food: FoodDTO): Promise<any>;
}
