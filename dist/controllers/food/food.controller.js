"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const food_service_1 = require("./food.service");
const foodDTO_1 = require("./DTO/foodDTO");
let FoodController = class FoodController {
    constructor(foodService) {
        this.foodService = foodService;
    }
    getFood() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.foodService.getFood();
        });
    }
    getFoodByID(idFood) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.foodService.getFoodById(idFood);
        });
    }
    createFood(food) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.foodService.createFood(food);
        });
    }
    deleteFood(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.foodService.deleteFood(id);
        });
    }
    updateFood(idFood, food) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.foodService.updateFood(idFood, food);
        });
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "getFood", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "getFoodByID", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [foodDTO_1.FoodDTO]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "createFood", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "deleteFood", null);
__decorate([
    common_1.Put(':idFood'),
    __param(0, common_1.Param('idFood')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, foodDTO_1.FoodDTO]),
    __metadata("design:returntype", Promise)
], FoodController.prototype, "updateFood", null);
FoodController = __decorate([
    common_1.Controller('food'),
    __metadata("design:paramtypes", [food_service_1.FoodService])
], FoodController);
exports.FoodController = FoodController;
//# sourceMappingURL=food.controller.js.map