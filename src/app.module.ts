import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FoodModule } from './controllers/food/food.module';
import { MongooseModule } from '@nestjs/mongoose';
import { SearchModule } from './controllers/search/search.module';

@Module({
  imports: [
    FoodModule,
    MongooseModule.forRoot('mongodb://localhost/foodRestaurant'),
    SearchModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
