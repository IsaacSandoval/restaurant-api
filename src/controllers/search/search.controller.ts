import { Controller, Get, Param } from '@nestjs/common';
import { SearchService } from './search.service';

@Controller('search')
export class SearchController {

    constructor(private searchService: SearchService) { }

    @Get(':termino')
    getResults(@Param('termino') termino: string) {
        return this.searchService.getResults(termino);
    }
    
}
