import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class SearchService {

    constructor(@InjectModel('Food') private readonly foodModel: Model<any>) { }

    async getResults(termino: string) {
        var regex = new RegExp(termino, 'i');
        const foods = await this.foodModel.find({}).or([{name: regex}]);
        return foods;
    }

}
