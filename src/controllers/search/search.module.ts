import { Module } from '@nestjs/common';
import { SearchService } from './search.service';
import { SearchController } from './search.controller';
import { FoodSchema } from '../food/schema/foodSchema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  providers: [SearchService],
  imports: [
    MongooseModule.forFeature([{ name: 'Food', schema: FoodSchema }])
  ],
  controllers: [SearchController]
})
export class SearchModule {}
