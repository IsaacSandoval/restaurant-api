import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FoodDTO } from './DTO/foodDTO';

@Injectable()
export class FoodService {

    constructor(@InjectModel('Food') private readonly foodModel: Model<any>) { }

    async getFood() {
        return await this.foodModel.find().exec();
    }

    async getFoodById(idFood: string) {
        return await this.foodModel.findOne({_id: idFood});
    }

    async createFood(food: FoodDTO) {
        const newFood = new this.foodModel(food);
        return await newFood.save();
    }

    async deleteFood(idFood: string) {
        return await this.foodModel.findOneAndDelete({_id: idFood});
    }

    async updateFood(idFood: string, foodUpdated: FoodDTO): Promise<any> {
        return await this.foodModel.findOneAndUpdate({_id: idFood}, foodUpdated);
    }

}
