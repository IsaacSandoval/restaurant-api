import { Module } from '@nestjs/common';
import { FoodService } from './food.service';
import { FoodController } from './food.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { FoodSchema } from './schema/foodSchema';

@Module({
  providers: [FoodService],
  imports: [
    MongooseModule.forFeature([{ name: 'Food', schema: FoodSchema }])
  ],
  controllers: [FoodController],
})
export class FoodModule { }
