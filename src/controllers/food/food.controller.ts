import { Controller, Post, Body, Get, Param, Delete, Put } from '@nestjs/common';
import { FoodService } from './food.service';
import { FoodDTO } from './DTO/foodDTO';

@Controller('food')
export class FoodController {

    constructor(private foodService: FoodService) {
    }

    @Get()
    async getFood() {
        return await this.foodService.getFood();
    }

    @Get(':id')
    async getFoodByID(@Param('id') idFood: string) {
        return await this.foodService.getFoodById(idFood);
    }

    @Post()
    async createFood(@Body() food: FoodDTO) {
        return await this.foodService.createFood(food);
    }

    @Delete(':id')
    async deleteFood(@Param('id') id: string) {
        return await this.foodService.deleteFood(id);
    }

    @Put(':idFood')
    async updateFood(@Param('idFood') idFood: string, @Body() food: FoodDTO) {
        return this.foodService.updateFood(idFood, food);
    }


}
