import * as mongoose from 'mongoose';

export const FoodSchema = new mongoose.Schema({
    name: String,
    price: Number,
    description: String,
    categorie: Number,
    img: String,
});
